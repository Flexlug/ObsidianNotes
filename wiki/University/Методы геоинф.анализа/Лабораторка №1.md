- Открыть в ERDAS 2022 файл tm_860516.img
- "Raster" -> "Subset and Chip" -> "Create Subset Image". Пересохранить файл с любым названием. Select layers указать "2:4".
- "Classification" -> "Supervised" -> "Feature Space Image". Выбрать "Color" -> "Output To Viewer". Нажать OK. "Multispectral" -> "Layer". Поменять Layer_4 и Layer_2 местами.
- "Classification" -> "Supervised" -> "Signature Editor". В новом окне выбрать "Feature" -> "Create" -> "Create Feature Space Images". Выбрать своё изображение в "Input Raset Layer". Levels Slice выбрать "Color". Установить "Output To Viewer". OK. Появится 3 окна цветных, показывающее распределение пикселей. Закрыть окно, в котором график выглядит как одна диагональная линия (ну илиблизка к ней). Оставим только 4ой viewer.
- "Signature Editor" -> "Feature" -> "View" -> "Linked Cursors". Выбрать Viewer №4. Нажать кнопку Link и ткнуть ЛКМ по открытой картинке на главном окне. Должна заработать привязка между графиком и главным изображением.
- Открываем Viewer_4. "AOI" -> "Tools...". Выделяем квадратиком голубую часть.
???

- "Unsupervised" -> "Unsupervised Classification". Галочку поставить на "Output signature set". ОТкрываем файл слева и справа с одинаковым именем "tm12c". Maximum Iterations - 30. Convergence Threshold - "0.990". Number of Classes: 12.
- Результаты обработки открыть.
	- .img открыть через главное окно "File" -> "Open"
	- .sig открыть через "Supervised" -> "Signature Editor". Там выбрать "File" -> "Open".
- "Display Mean Plot Window". Смотрим на полоски. Делаем умный вид. Ставим "такие" цвета (короче какие-нибудь).
- Через выделение выделим все строки. Далее ПКМ по стобцу Color -> "Edit" -> "Copy". Далее переходим в главное окно. Жмем на tm12c.img в таблице. Далее "Table" -> "Show Attributes". Открывается таблица снизу в главном окне. Также все выделяем через выделение с 1 по 12. ПКМ по столбцу Color -> "Edit" -> "Paste".
