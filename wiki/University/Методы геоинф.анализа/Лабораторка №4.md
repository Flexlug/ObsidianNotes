Открыть файл из прошлой работы `36cto4.img`.

Raster -> Thematic -> Oberlay by Min or Max
Setup Recode

| Номер строки | Значение |
| ------------ | -------- |
| 0            | 0        |
| 1            | 10       |
| 2            | 0        |
| 3            | 8        |
| 4            | 9        |

Choose value to determinate: Maximum Value
Output file: overlay.img
OK

Table -> Show Attributes -> Скопировать цвета с 1 по 7 в f7c.img. 
Выбрать файл overlay.img
Table -> Show Attributes -> Вставить цвета с 1 по 7


Далее по этому избражению мы сейчас будем строить зональные атрибуты.
Raster -> Thematic -> Zonal Attributes

Откорем векторный Shapefile result_4c.shp.

Format -> Verctor Symbology -> Viewing Properties
All -> выставить на белый цвет (Outlined White)

Raster -> Zonal Attributes
Vector layer: result_4c.shp
Raster alayer: overlay.img

Majority -> Attribute Name: "MAIN_CLASS"
OK

Далее нужно result_4c.shp закрыть и заново открыть.
Table -> Show Attributes

5ый класс - береза
7ой класс - нарушенные леса
Теперь нам интересно получить нарушенность по каждому контуру, но если мы нарушеннось будем считать по этому, то здесь у нас не получится, потому что нам мешает застройка (это всё отдельные контуры), а вот в пределах сельскохозяйственной местности нам было бы интеренсо посчитать застройку. Поэтому "нам необходимо сделать хитрость".

Мы возьмем 36c4c.img и сделаем из него бинарный файл, где у нас будет нарушенность. 
Сделаем маску
Raster -> Thematic -> Recode

Input File: 36c4c.img
Output File: bin1.img
| Номер строки | Значение |
| ------------ | -------- |
| 0            | 0        |
| 1-3          | 0        |
| 4            | 1        |

Пследняя - нарушенность. 

OK

Откроем bin1.img
bin1 в Contents перетащим ниже. Под result_4c.shp, над overlay.img

Table -> Show Attributes

Далее мы сделаем вторую маску. Сперва мы делали нарушенность лесов по застройке. Сделаем теперь по лесам отдельно.
Сперва перекрасим только что созданны шейп

Format -> Vieweing Properties 
"Изменила контур на другой цвет"
Outline

Далее мы нарушенность попробуем найти по весам.
Raster -> Thematic -> Recode
Input file: f7c.img

Setup Recode:
| номер строки | значение |
| ------------ | -------- |
| 0            | 0        |
| 1-6          | 0        |
| 7            | 1        |
Output file: bin2.img
OK

Откроем bin2.img
Выделим в Contents bin2.img
Table -> Show Attributes
| Rows | Color  |
| ---- | ------ |
| 0    | Black  |
| 1    | Yellow |

Raster -> Thematic -> Overlay
Image of Vector File №1: bin1.img
Image of Vector File №2: bin2.img
Output file: destroy.img
OK

Откроем файл destroy.img

В итоге мы поулчили полную нарушенность по всей контурам.
В первом случаеу у нас не было дачного участка, который был в лесу. А когда мы отдельно классифицировали леса, то у нас вылезли эти дачные постройки.
Если мы детальено хотим классифицировать какую-то группу объектов, то нам сперва нужно на эти обхекты наложить маску, и только после этого мы можем выделить группу объектов. 


Теперь мы можем по этому бинарному изображению посчитать процент нарушенности на каждый контур.

Теперь мы должны взять зональные атрибуты и посчитать среднее
Raster -> Thematic -> Zonal Attributes
Vector Layer: resut_4c.ssh
Raster Layer: destroy.img

Zonal Functions:
Mean: DESTROY
OK

После того, как посчитает, нам необходимо сперва закрыть result_4c.shp, а потом открыть его. 

Сейчас попробую найти то, что нам нужно, для того чтобы можно было посчитать загрязнение.


Raster -> Thematic -> Zonal Attributes
Vector layer: result_4c
Raster Layer: zagr-1

Zonal Functions:
Mean: B_POLL
OK

![](https://gitlab.com/Flexlug/ObsidianNotes/-/raw/main/wiki/University/%D0%9C%D0%B5%D1%82%D0%BE%D0%B4%D1%8B%20%D0%B3%D0%B5%D0%BE%D0%B8%D0%BD%D1%84.%D0%B0%D0%BD%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0/1.JPG?inline=false)


![](https://gitlab.com/Flexlug/ObsidianNotes/-/raw/main/wiki/University/%D0%9C%D0%B5%D1%82%D0%BE%D0%B4%D1%8B%20%D0%B3%D0%B5%D0%BE%D0%B8%D0%BD%D1%84.%D0%B0%D0%BD%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0/2.JPG?inline=false)


![](https://gitlab.com/Flexlug/ObsidianNotes/-/raw/main/wiki/University/%D0%9C%D0%B5%D1%82%D0%BE%D0%B4%D1%8B%20%D0%B3%D0%B5%D0%BE%D0%B8%D0%BD%D1%84.%D0%B0%D0%BD%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0/3.JPG?inline=false)

![](https://gitlab.com/Flexlug/ObsidianNotes/-/raw/main/wiki/University/%D0%9C%D0%B5%D1%82%D0%BE%D0%B4%D1%8B%20%D0%B3%D0%B5%D0%BE%D0%B8%D0%BD%D1%84.%D0%B0%D0%BD%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0/4.JPG?inline=false)

![](https://gitlab.com/Flexlug/ObsidianNotes/-/raw/main/wiki/University/%D0%9C%D0%B5%D1%82%D0%BE%D0%B4%D1%8B%20%D0%B3%D0%B5%D0%BE%D0%B8%D0%BD%D1%84.%D0%B0%D0%BD%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0/5.JPG?inline=false)

![](https://gitlab.com/Flexlug/ObsidianNotes/-/raw/main/wiki/University/%D0%9C%D0%B5%D1%82%D0%BE%D0%B4%D1%8B%20%D0%B3%D0%B5%D0%BE%D0%B8%D0%BD%D1%84.%D0%B0%D0%BD%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0/6.JPG?inline=false)

![](https://gitlab.com/Flexlug/ObsidianNotes/-/raw/main/wiki/University/%D0%9C%D0%B5%D1%82%D0%BE%D0%B4%D1%8B%20%D0%B3%D0%B5%D0%BE%D0%B8%D0%BD%D1%84.%D0%B0%D0%BD%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0/7.JPG?inline=false)

![](https://gitlab.com/Flexlug/ObsidianNotes/-/raw/main/wiki/University/%D0%9C%D0%B5%D1%82%D0%BE%D0%B4%D1%8B%20%D0%B3%D0%B5%D0%BE%D0%B8%D0%BD%D1%84.%D0%B0%D0%BD%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0/8.JPG?inline=false)

![](https://gitlab.com/Flexlug/ObsidianNotes/-/raw/main/wiki/University/%D0%9C%D0%B5%D1%82%D0%BE%D0%B4%D1%8B%20%D0%B3%D0%B5%D0%BE%D0%B8%D0%BD%D1%84.%D0%B0%D0%BD%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0/9.JPG?inline=false)

