Источники: 
https://docs.nextgis.ru/docs_ngweb_dev/doc/admin/install.html
https://docs.nextgis.ru/docs_ngweb_dev/doc/admin/install.html

# Начальная настройка VM

## ВСЕ МАХИНАЦИИ БЫЛИ ПРОТЕСТИРОВАНЫ НА ЧИСТОЙ UBUNTU 20.04 SERVER. 
В случае чего пишите мне.
[ссылка на дистрибутив](https://releases.ubuntu.com/20.04/ubuntu-20.04.4-live-server-amd64.iso)

![Картинка 1](https://gitlab.com/Flexlug/ObsidianNotes/-/raw/main/wiki/University/%D0%93%D0%B5%D0%BE%D0%BF%D0%BE%D1%80%D1%82%D0%B0%D0%BB%D1%8B/Pasted%20image%2020220416015547.png?inline=false)

Заходим в "Проброс портов". Устанавливаем следующие настройки.
Также добавляем виртуальный адаптер хоста. Глубже в настройки не лезем.

![Картинка 2](https://gitlab.com/Flexlug/ObsidianNotes/-/raw/main/wiki/University/%D0%93%D0%B5%D0%BE%D0%BF%D0%BE%D1%80%D1%82%D0%B0%D0%BB%D1%8B/Pasted%20image%2020220416030120.png?inline=false)

Добавляем 20ую убунту в качестве USB носителя.

![Картинка 3](https://gitlab.com/Flexlug/ObsidianNotes/-/raw/main/wiki/University/%D0%93%D0%B5%D0%BE%D0%BF%D0%BE%D1%80%D1%82%D0%B0%D0%BB%D1%8B/Pasted%20image%2020220416015701.png?inline=false)

Запускаем VM.

В установщике выбираем ENGLISH
Жмем везде ENTER до момента, где предлагается ввести имя/логин и т.п.

| Параметр              | Значение   |
| --------------------- | ---------- |
| Your name:            | ngwserver |
| Your server's name:   | ngwserver  |
| Pick a username:      | ngwserver  |
| Choose a password:    | 123456789  |
| Confirm you password: | 123456789  |

> Такой пароль был выбран из соображений быстроты и удобства ввода при многократном вызове команд `sudo` и т.п. Очевидно, что такой пароль нельзя в продакшене ставить. (и вообще нужно root вырубить, и ssh только по ключам и т.п., но мы над этим не паримся)

В следующем окне, где предлагается предустановить сервисы, ставим галочку на "Install OpenSSH server".
В следующем окне "Featured Server Snaps" ничего не ставим.

Ждем конца установки. Выключаем виртуальную машину. Отключаем установачный образ Ubuntu. Запускаем виртуалку. <u>Логиниться в ней необязательно</u>.

Теперь открываем терминал в Windows (ну или PowerShell или CMD. В зависимости от того, что у вас есть. Разницы нет). Вписываем команду:

```bash
ssh ngwserver@localhost -p 2222
```

Вводим пароль. 
Всё. Мы подключились к виртуалке и теперь можем спокойно выполнять все манипуляции через нормальный терминал. 
В командную строку можно спокойно копировать все команды (через правую кнопку мыши). И из неё же можно копировать вывод. Если вдруг понадобится на виртуалку что-либо закинуть, то можно установить на компьютер WinSCP и через него скидывать все файлы (правда нам в этом гайде это не понадобится, поэтому здесь мы с WinSCP работать не будем).

# Установка зависимостей NextGIS Web
> Далее подразумевается, что вы каждый блок полностью копируете и целиком вставляете в терминал. Если вы хотите выполнить каждую команду по очереди, то копируйте каждую строчку без `&&` на конце.

Создадим нужные папки. Перейдем в них.
```bash
mkdir -p ~/ngw/{data,upload} &&
cd ~/ngw
```

Обновим все пакеты
```bash
sudo apt update -y &&
sudo apt upgrade -y
```

Доустановим необходимый пакет `add-apt-repository`
```bash
sudo apt install software-properties-common
```

Далее нам нужно добавить нужные репозитории, откуда будут качаться нужные пакеты. Залогинимся под учёткой `root`.
```bash
sudo -i
```

И выполним следующие команды:
```bash
curl --silent https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - &&
add-apt-repository --yes --no-update "deb https://deb.nodesource.com/node_14.x $(lsb_release -sc) main" &&
curl --silent https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - &&
add-apt-repository --yes --no-update "deb https://dl.yarnpkg.com/debian/ stable main"
```
Должно 2 раза высветить OK.

Вернемся в учётку ngwserver. После выполнения команды `su` вы скорее всего окажитесь в папке `/root`, поэтому нам нужно вернуться назад в директорию `/home/ngwserver/ngw`. 
```bash
su ngwserver &&
cd ~/ngw/
```

Снова на всякий случай проверим пакеты
```bash
sudo apt update -y &&
sudo apt upgrade -y
```

Теперь устанавливаем последний доступный PosgtreSQL
```bash
sudo apt install postgresql -y &&
sudo -u postgres createuser ngw_admin -P -e
```

Вам предложит ввести пароль для администратора БД. Введем `123456789`. Это нужно будет запомнить на будущее.
Имя пользователя БД: `ngw_admin`.

```bash
sudo -u postgres createdb -O ngw_admin --encoding=UTF8 db_ngw &&
sudo service postgresql restart &&
sudo apt install postgresql-12-postgis-3 postgresql-12-postgis-3-scripts -y &&
sudo -u postgres psql -d db_ngw -c 'CREATE EXTENSION postgis;' &&
sudo -u postgres psql -d db_ngw -c 'ALTER TABLE geometry_columns OWNER TO ngw_admin;' &&
sudo -u postgres psql -d db_ngw -c 'ALTER TABLE spatial_ref_sys OWNER TO ngw_admin;' &&
sudo -u postgres psql -d db_ngw -c 'ALTER TABLE geography_columns OWNER TO ngw_admin;' &&
sudo -u postgres psql -d db_ngw -c 'CREATE EXTENSION hstore;' &&
psql -h localhost -d db_ngw -U ngw_admin -c "SELECT PostGIS_Full_Version();" 
```
Может спросить пароль в конце.

В итоге имеем следующие данные про БД:

| Поле        | Значение  |
| ----------- | --------- |
| Название БД | db_ngw    |
| Имя админа  | ngw_admin |
| Пароль      | 123456789 |

Далее ещё раз убедимся, что мы находимся в нужной директории. Установим пакет, который позволяет создавать виртуальные среды.
```bash
cd ~/ngw
sudo apt install python3-virtualenv -y &&
```

Войдём в виртуальную среду. Т.к. нам придется устанавливать килитонну Python пакетов, нам желательно иметь изолированную среду, в пределах которой будут находиться все требуемые зависимости. 
Наша виртуальная среда будет называться `env`.
```bash
virtualenv env &&
source ./env/bin/activate
```

Теперь начнем потихонечку разгребать этот dependency hell.
К сожалению некоторые зависимости нужно устанавливать в ручном режиме, ибо некоторые последние версии несовместимы друг с другом. Одни из таких завимимостей: `psycopg2` и `pygdal`. Нам нужно установить чётко нужную версию.
```bash
sudo apt install libgdal-dev python3-dev libpq-dev g++ libqgis-dev qt5-image-formats-plugins build-essential cmake libssl-dev libgeos-dev gdal-bin libxml2-dev libxslt1-dev zlib1g-dev libjpeg-turbo8-dev postgresql-client libmagic-dev nodejs yarn -y &&
pip3 install psycopg2 &&
pip3 install pygdal==3.0.4.10
```
Всё будет происходить очень медленно, т.к. тут разом устанавливается почти всё, что вообще в будущем нам понадобится. Так что спокойно уйти попить чай .

Скачаем исходники NextGIS Web. Установим его.
```bash
git clone https://github.com/nextgis/nextgisweb.git &&
./env/bin/pip install -e ./nextgisweb

```

Далее установим одну из зависимостей - Mapscript.
Устанавливается она через костыль:
- Сперва Mapscript просто устаналиваем на компьютер через утилиту `apt`;
- Далее вручную мы копируем нужные файлы из установленного Mapscript в папку с виртуальной средой. 
```bash
sudo apt install python3-distutils python3-mapscript -y &&
cp -r /usr/lib/python3/dist-packages/*mapscript* ~/ngw/env/lib/python3.8/site-packages/ &&
echo "./mapscript.egg" > ~/ngw/env/lib/python3.8/site-packages/mapscript.pth
```

Введите на всякий случай следующую команду:
```
env/bin/pip freeze
```

Если вам вывалит гигантский список установленных пакетов, значит всё в порядке. В противном случае гуглим ошибку (может быть недоустановлен какой-нибудь пакет).
Проверьте, есть ли в этом списке пакет `mapscript`.
Там будет что-то такое
```
...
iniconfig==1.1.1
lxml==4.6.4
Mako==1.2.0
mapscript==7.4.3    <--- Нужный пакет
MarkupSafe==2.1.1
mccabe==0.6.1
networkx==2.8
...

```

Далее скачаем и установим `qgis_headless`.

```bash
git clone https://github.com/nextgis/qgis_headless.git &&
pip install -e ./qgis_headless/[tests]
```

Скачем и установим NextGIS Web QGis

```bash
git clone https://github.com/nextgis/nextgisweb_qgis.git &&
pip install -e nextgisweb_qgis/
```

Скачаем и установим NextGIS Web Mapserver

```bash
git clone https://github.com/nextgis/nextgisweb_mapserver.git &&
./env/bin/pip install -e ./nextgisweb_mapserver
```

Если вы дошли до сюда, то вы закончили самую противную часть
работы.

# Настройка NextGIS Web

Сгенерируем начальный конфиг
```bash
env/bin/nextgisweb-config > config.ini
```

И откроем его
```
nano config.ini
```

Что тут нам обязательно нужно поменять:
| Поле              | Значение                 |
| ----------------- | ------------------------ |
| database.host     | localhost                |
| database.name     | db_ngw                   |
| database.user     | ngw_admin                |
| database.password | 123456789                |
| sdir              | /home/ngwserver/ngw/data |

Не забудьте раскомментировать заданные вами параметры (уберите `;` в начале строки). Остальные не трогайте.

Проверьте наличие папки `data` в директории `/home/ngwserver/ngw`
```bash
ls ~/ngw/
```

Если таковой не будет - создайте её. Иначе пропускаете эту команду
```bash
mkdir data
```

# Финальные штрихи

Добавим в системный PATH переменную `NEXTGISWEB_CONFIG`.
```bash
export NEXTGISWEB_CONFIG=/home/ngwserver/ngw/config.ini
```

***!!!!!! АХТУНГ !!!!!!***
Если вам при запуске nextgisserver выдаёт примерно такую хрень:
```
Traceback (most recent call last):
  File "env/bin/nextgisweb", line 3, in <module>
    if __name__ == '__main__': main()
  File "/home/ngwserver/ngw/nextgisweb/nextgisweb/script.py", line 59, in main
    env.initialize()
  File "/home/ngwserver/ngw/nextgisweb/nextgisweb/env.py", line 143, in initialize
    c.initialize()
  File "/home/ngwserver/ngw/nextgisweb/nextgisweb/render/__init__.py", line 60, in initialize
    self.tile_cache_path = os.path.join(self.env.core.gtsdir(self), 'tile_cache')
  File "/usr/lib/python3.8/posixpath.py", line 76, in join
    a = os.fspath(a)
TypeError: expected str, bytes or os.PathLike object, not NoneType
```
То просто ещё раз пропишите `export`, при этом проверьте лишний раз, правильно ли там указан путь к файлу `config.ini`.

Установим ещё одну зависимость для NextGIS Web
```bash
env/bin/nextgisweb jsrealm.install &&
mkdir dist &&
yarn run build
```
Если что, `yarn run build` будет висеть относительно долго. Не пугаемся.

Теперь инициализиурем БД (точнее привяжем NextGIS Web к БД)
```bash
env/bin/nextgisweb initialize_db
```

И, наконец, запустим его
```bash
env/bin/nextgisweb server
```

Теперь с нашего основного компьютера переходим по следующему адресу: http://localhost:8080

Если всё получилось, то мы увидим это
![Картинка 4](https://gitlab.com/Flexlug/ObsidianNotes/-/raw/main/wiki/University/%D0%93%D0%B5%D0%BE%D0%BF%D0%BE%D1%80%D1%82%D0%B0%D0%BB%D1%8B/Pasted%20image%2020220416051903.png?inline=false)

Логин: `administrator`
Пароль: `admin`

Мы попадем в админку NextGIS Web
![Картинка 5](https://gitlab.com/Flexlug/ObsidianNotes/-/raw/main/wiki/University/%D0%93%D0%B5%D0%BE%D0%BF%D0%BE%D1%80%D1%82%D0%B0%D0%BB%D1%8B/Pasted%20image%2020220416051932.png?inline=false)

NextGIS QGIS тоже работает. Спокойно коннектится.

![Картинка 6](https://gitlab.com/Flexlug/ObsidianNotes/-/raw/main/wiki/University/%D0%93%D0%B5%D0%BE%D0%BF%D0%BE%D1%80%D1%82%D0%B0%D0%BB%D1%8B/Pasted%20image%2020220416052226.png?inline=false)

Вот и всё.